var flight = require('flightjs');

function productLink() {

    this.attributes({
        link: '.' + 'b-link'
    });

    this.after('initialize', function () {
        this.on('click', {
            link: this.handleClick
        });
    });

    this.handleClick = function (event) {
        event.preventDefault();
        this.trigger('uiProductVisibleSelected');
    };
}

module.exports = flight.component(productLink);