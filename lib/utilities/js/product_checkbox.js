var flight = require('flightjs');

function productCheckbox() {

    this.attributes({
        checkbox:             '.' + 'b-checkbox',
        checkboxFormFild:     '.' + 'b-form-field_type_checkbox',
        checkboxStateDisabledClass: 'b-checkbox_state_disabled'
    });

    this.after('initialize', function () {
        this.checkState();

        this.on('click', {
            checkbox: this.toggleState
        });
        this.on('uiProductSelected', {
            checkbox: this.validateState
        });
        this.on('uiProductUnSelected', {
            checkbox: this.validateState
        });
    });

    this.toggleState = function (event) {
        if (event.target.type === 'checkbox') {
            this.checkState(event);
        }
    };

    this.checkState = function (event) {
        if(this.select('checkboxFormFild').prop('checked')) {
            this.trigger('uiProductVisibleSelected');
        }
        else {
            this.trigger('uiProductVisibleNoSelected');
        }
    };

    this.validateState = function (event) {
        if ((event.type === 'uiProductSelected' && !this.select('checkboxFormFild').prop('checked')) ||
            (event.type === 'uiProductUnSelected' && this.select('checkboxFormFild').prop('checked'))
            ) {
            this.setState(event);
        }
    };

    this.setState = function (event) {
        if (event.target.type !== 'checkbox') {
            if(this.select('checkboxFormFild').prop('checked')) {
                this.select('checkboxFormFild').prop('checked', false);
            }
            else {
                this.select('checkboxFormFild').prop('checked', true);
            }
        }
    };
}

module.exports = flight.component(productCheckbox);