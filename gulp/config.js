var path = require('path');

var dest = './production';
var src = './lib';
var relativeSrcPath = path.relative('.', src);

module.exports = {
    src: src,
    dest: dest,
    jade: {
        src: [
            src + '/pages/**/!(_)*.jade'
        ],
        dest: dest,
        options: {pretty: true}
    },
    js: {
        src: src + '/js/**',
        dest: dest + '/js',
        uglify: true
    },
    webpack: {
        entry: src + '/utilities/js/main.js',
        output: {
          filename: 'bundle.js'
        },
        resolve: {
          extensions: ['', '.js']
        }
    },
    stylus: {
        src: [
            src + '/utilities/styl/**/!(_)*.bundle.styl'
        ],
        dest: dest + '/css/',
        output: 'main.css',
        autoprefixer: {
            browsers: ['last 2 versions']
        },
        minify: false
    },
    copy: {
        src: [
            src + '/assets/**'
        ],
        dest: dest
    },
    watch: {
        js: [
            relativeSrcPath + '/utilities/js/**',
            relativeSrcPath + '/blocks/**/*.js'
        ],
        styl: [
            relativeSrcPath + '/utilities/styl/**',
            relativeSrcPath + '/blocks/**/*.styl'
        ],
        jade: [
            relativeSrcPath + '/utilities/jade/**',
            relativeSrcPath + '/blocks/**/*.jade',
            relativeSrcPath + '/pages/**'
        ],
        assets: relativeSrcPath + '/assets/**'
    }
};