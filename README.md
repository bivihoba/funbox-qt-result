# Funbox QT

* Project based on Gulp.
* Jade for templating.
* BEM-like file structure.
* Hand dependencies for CSS (it would be overkill to write a dependencies compiler).
* Flight framework for Javascript components
