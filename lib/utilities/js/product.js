var flight = require('flightjs');

function product() {

    this.attributes({
        product:                      '.' + 'b-product',
        productPack:                  '.' + 'b-product__pack',
        productCheckbox:              '.' + 'b-product__checkbox',
        productSelectedClass:               'b-product_selected_true',
        productOutOfStockClass:             'b-product_out-of-stock_true'
    });

    this.after('initialize', function () {
        this.on('uiProductVisibleSelected', {
            product: this.setStateSelected
        });
        this.on('uiProductVisibleNoSelected', {
            product: this.setStateUnSelected
        });
    });

    this.setStateSelected = function (event) {
        if (!this.$node.hasClass(this.attr.productOutOfStockClass)) {
            if (!this.$node.hasClass(this.attr.productSelectedClass)) {
                this.$node.addClass(this.attr.productSelectedClass);
                this.select('productPack').trigger('uiProductSelected');
                this.select('productCheckbox').trigger('uiProductSelected');
            }
        }
    };

    this.setStateUnSelected = function (event) {
        if (!this.$node.hasClass(this.attr.productOutOfStockClass)) {
            if (this.$node.hasClass(this.attr.productSelectedClass)) {
                this.$node.removeClass(this.attr.productSelectedClass);
                this.select('productPack').trigger('uiProductUnSelected');
                this.select('productCheckbox').trigger('uiProductUnSelected');
            }
        }
    };
}

module.exports = flight.component(product);