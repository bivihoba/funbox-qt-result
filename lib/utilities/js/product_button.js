var flight = require('flightjs');

function productButton() {

    this.attributes({
        button:                '.' + 'b-button',
        buttonHoverStateClass:       'b-button_hover_true',
        buttonActiveStateClass:      'b-button_active_true',
        buttonLockedFalseStateClass: 'b-button_locked_false',
        buttonLockedTrueStateClass:  'b-button_locked_true'
    });

    this.after('initialize', function () {
        this.on('mouseover', {
            button: this.setHoverState
        });
        this.on('mouseout', {
            button: this.removeHoverState
        });
        this.on('uiProductSelected', {
            button: this.setActiveState
        });
        this.on('uiProductUnSelected', {
            button: this.removeActiveState
        });
    });

    this.setHoverState = function (event) {
        this.$node.addClass(this.attr.buttonHoverStateClass);
    };

    this.removeHoverState = function (event) {
        this.$node.removeClass(this.attr.buttonHoverStateClass);
        this.removeLockedState();
    };

    this.setActiveState = function (event) {
        this.$node.addClass(this.attr.buttonActiveStateClass);
        this.setLockedState();
    };

    this.removeActiveState = function (event) {
        this.$node.removeClass(this.attr.buttonActiveStateClass);
        this.setLockedState();
    };

    this.setLockedState = function (event) {
        if (this.$node.hasClass(this.attr.buttonHoverStateClass)) {
            this.$node.removeClass(this.attr.buttonLockedFalseStateClass);
            this.$node.addClass(this.attr.buttonLockedTrueStateClass);
        }
    };

    this.removeLockedState = function (event) {
        if (!this.$node.hasClass(this.attr.buttonHoverStateClass)) {
            this.$node.removeClass(this.attr.buttonLockedTrueStateClass);
            this.$node.addClass(this.attr.buttonLockedFalseStateClass);
        }
    };
}

module.exports = flight.component(productButton);