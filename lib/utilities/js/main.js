var flight = require('flightjs');
var product = require('./product');
var productButton = require('./product_button');
var productCheckbox = require('./product_checkbox');
var productLink = require('./product_link');

    product.attachTo('.b-product_viewtype_tile');
    productButton.attachTo('.b-product_viewtype_tile .b-product__pack.b-button');
    productCheckbox.attachTo('.b-product_viewtype_tile .b-product__content .b-checkbox');
    productLink.attachTo('.b-product_viewtype_tile .b-product__handle.b-link');
